# PyBot #

This is a simple lib of pageObject/pageFactory concept to create scripts with Python and Selenium

### Requirements ###

* Python 3
* pip
* [Selenium](https://pypi.python.org/pypi/selenium)

### How do I get set up? ###

* clone the repository
    `git clone https://bitbucket.org/pybot/pybot.git`

* Send the lib to you dist-package
    `pip install .`

* Import on your script
    `import pybot`


### Example: ###


```Python

from pybot.core.manager import Manager
from pybot.core.pageObject import PageObject
from pybot.core.pageElement import PageElement, PageElements


class GooglePage(PageObject):
    search = PageElement(name='q')
    link   = PageElement(link_text='Staggering Beauty')


manager = Manager()
manager.go('http://google.com')

page = GooglePage(manager)
page.search = 'staggeringbeauty'
page.link.click()

#manager.end()

```

>More samples on https://bitbucket.org/pybot/pybot_scripts
