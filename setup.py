from setuptools import setup, find_packages


setup(name='pybot',
      version='1.0',
      description='Simple Page Object/Factory package on python',
      author='Felipe Viegas',
      license='MIT',
      packages=find_packages(),
  )