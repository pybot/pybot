from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException


class PageElement:
    _LOCATOR_MAP = {'css': By.CSS_SELECTOR,
                    'id_': By.ID,
                    'name': By.NAME,
                    'xpath': By.XPATH,
                    'link_text': By.LINK_TEXT,
                    'partial_link_text': By.PARTIAL_LINK_TEXT,
                    'tag_name': By.TAG_NAME,
                    'class_name': By.CLASS_NAME,
                    }

    def __init__(self, context=False,frame= None,**kwargs):
        if not kwargs:
            raise ValueError("Please specify a locator")

        if len(kwargs) > 1:
            raise ValueError("Please specify only one locator")

        k, v = next(iter(kwargs.items()))

        self.locator = (self._LOCATOR_MAP[k], v)
        self.has_context = bool(context)
        self.frameSet = frame

    def find(self, manager):
        try:
            manager.drive_to_frame(self.frameSet)
            return manager.find_component(*self.locator)
        except Exception as e:
            print(e)
            return None

    def __get__(self, instance, owner, context=None):
        if not instance:
            return None

        if not context and self.has_context:
            return lambda ctx: self.__get__(instance, owner, context=ctx)

        if not context:
            context = instance.manager

        return self.find(context)

    def __set__(self, instance, value):
        if self.has_context:
            raise ValueError("Sorry, the set descriptor doesn't support elements with context.")
        elem = self.__get__(instance, instance.__class__)
        if not elem:
            raise ValueError("Can't set value, element not found")
        elem.set(value)

    def __setitem__(self, key, value):
        pass
        print('a')
        pass

    def __getitem__(self, item):
        pass
        print('b')
        pass

class PageElements(PageElement):
    def find(self, manager):
        try:
            manager.drive_to_frame(self.frameSet)
            return manager.find_components(*self.locator)
        except Exception as e:
            print(e)
            return []

    def __set__(self, instance, value):
        if self.has_context:
            raise ValueError("Sorry, the set descriptor doesn't support elements with driver.")
        elems = self.__get__(instance, instance.__class__)
        if not elems:
            raise ValueError("Can't set value, no elements found")
        [elem.set(value) for elem in elems]

    def __setitem__(self, key, value):
        pass
        print('a')
        pass

    def __getitem__(self, item):
        pass
        print('b')
        pass
