from selenium import webdriver
from pybot.core.component import Element, Select
from pybot.core import configuration
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


class Manager:
    def __init__(self):

        configuration.init()

        profile = configuration.getConfig('Manager', 'profile')
        timeout = int(configuration.getConfig('Manager', 'timeout'))
        maximize = bool(configuration.getConfig('Manager', 'maximize'))

        self.driver = self.setProfile(profile)
        self.driver.implicitly_wait(timeout)
        if maximize:
            self.driver.maximize_window()

    def end(self):
        # logger.close()  # finaliza com msg de log
        self.driver.quit()

    def setProfile(self, profile):
        if profile.lower() == 'Firefox'.lower():
            return webdriver.Firefox()
        elif profile.lower() == 'FirefoxProfile'.lower():
            return webdriver.FirefoxProfile()
        elif profile.lower() == 'Chrome'.lower():
            return webdriver.Chrome()
        elif profile.lower() == 'ChromeOptions'.lower():
            return webdriver.ChromeOptions()
        elif profile.lower() == 'Ie'.lower():
            return webdriver.Ie()
        elif profile.lower() == 'Opera'.lower():
            return webdriver.Opera()
        elif profile.lower() == 'PhantomJS'.lower():
            return webdriver.PhantomJS()
        elif profile.lower() == 'Remote'.lower():
            return webdriver.Remote()
        elif profile.lower() == 'DesiredCapabilities'.lower():
            return webdriver.DesiredCapabilities()
        elif profile.lower() == 'ActionChains'.lower():
            return webdriver.ActionChains()
        elif profile.lower() == 'TouchActions'.lower():
            return webdriver.TouchActions()
        elif profile.lower() == 'Proxy'.lower():
            return webdriver.Proxy()
        else:
            return webdriver.Firefox()

    def go(self, url):
        self.driver.get(url)

    def find_component(self, by=By.ID, value=None):
        self.verify_element(by, value, 5)
        e = self.driver.find_element(by, value)
        return self.element_type(e)

    def find_components(self, by=By.ID, value=None):
        self.verify_element(by, value, 5)
        es = self.driver.find_elements(by, value)
        return [self.element_type(e) for e in es]

    def verify_element(self, by, locator, timeout=10):
        try:
            WebDriverWait(self.driver, timeout).until(
                ec.presence_of_element_located((by, locator)))
            return True
        except Exception:
            return False

    def element_type(self, elem):
        if elem.tag_name.lower() == "select":
            return Select(elem)
        else:
            return Element(elem)

    def drive_to_frame(self, frame_list):
        self.driver.switch_to_default_content()
        if frame_list is not None:
            for frame in frame_list.split():
                self.driver.switch_to_frame(frame)

    def __get__(self, instance, owner):
        return self.driver
