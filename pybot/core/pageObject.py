class PageObject:
    def __init__(self, manager, root_uri=None):
        self.manager = manager
        self.root_uri = root_uri if root_uri else getattr(self.manager.driver, 'root_uri', None)

    def go(self, uri):
        root_uri = self.root_uri or ''
        self.manager.driver.get(root_uri + uri)
