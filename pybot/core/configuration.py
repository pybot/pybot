import configparser


#Default configuration for each module
config_json = {
    'Manager': {
        'profile': 'firefox',
        'maximize': 'True',
        'timeout': '0'
    }
}


def init(file=None):

    global config_json

    config = configparser.ConfigParser()
    if file is None:
        file = 'pybot.ini'
    config.read(file)

    defaultConfiguration(config, config_json)

    with open(file, 'w') as configfile:
        config.write(configfile)


def defaultConfiguration(config, json):

    for section in json:
        # Creation of Section
        if section not in config:
            config.add_section(section)

        for param in json[section]:

            sec = config[section]

            # Creation of each param of Section
            if param not in sec:
                sec[param] = json[section][param]


def getConfig(*args):
    config = configparser.ConfigParser()
    file = 'pybot.ini'
    config.read(file)
    try:
        return config.get(*args)
    except:
        return None
