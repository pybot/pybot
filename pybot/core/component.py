from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.select import Select as Selenium_Select
from selenium.webdriver.common.by import By



class Element:
    def __init__(self, element):
        self.element = element

    def __setitem__(self, key, value):
        self.element[key].set(value)

    def click(self):
        self.element.click()

    def send_keys(self, value):
        self.element.clear()
        self.element.send_keys(value)

    def set(self, value):
        self.send_keys(value)

    def __set__(self, instance, value):
        self.set(value)

    def __get__(self, instance, owner, context=None):
        return self

    def tag_name(self):
        return self.element.tag_name()

    def text(self):
        return self.element.text()

    def submit(self):
        self.element.submit()

    def clear(self):
        self.element.clear()

    def get_attribute(self, name):
        return self.element.get_attribute(name)

    def is_selected(self):
        return self.element.is_selected()

    def is_enabled(self):
        return self.element.is_enabled()

    def find_element_by_id(self, id_):
        return self.element.find_element_by_id(id_)

    def find_elements_by_id(self, id_):
        return self.element.find_elements_by_id(id_)

    def find_element_by_name(self, name):
        return self.element.find_element_by_name(name)

    def find_elements_by_name(self, name):
        return self.element.find_elements_by_name(name)

    def find_element_by_link_text(self, link_text):
        return self.element.find_element_by_link_text(link_text)

    def find_elements_by_link_text(self, link_text):
        return self.element.find_elements_by_link_text(link_text)

    def find_element_by_partial_link_text(self, link_text):
        return self.element.find_element_by_partial_link_text(link_text)

    def find_elements_by_partial_link_text(self, link_text):
        return self.element.find_elements_by_partial_link_text(link_text)

    def find_element_by_tag_name(self, name):
        return self.element.find_element_by_tag_name(name)

    def find_elements_by_tag_name(self, name):
        return self.element.find_elements_by_tag_name(name)

    def find_element_by_xpath(self, xpath):
        return self.element.find_element_by_xpath(xpath)

    def find_elements_by_xpath(self, xpath):
        return self.element.find_elements_by_xpath(xpath)

    def find_element_by_class_name(self, name):
        return self.element.find_element_by_class_name(name)

    def find_elements_by_class_name(self, name):
        return self.element.find_elements_by_class_name(name)

    def find_element_by_css_selector(self, css_selector):
        return self.element.find_element_by_css_selector(css_selector)

    def find_elements_by_css_selector(self, css_selector):
        return self.element.find_elements_by_css_selector(css_selector)

    def is_displayed(self):
        return self.element.is_displayed()

    def location_once_scrolled_into_view(self):
        return self.element.location_once_scrolled_into_view()

    def size(self):
        return self.element.size()

    def value_of_css_property(self, property_name):
        return self.element.value_of_css_property(property_name)

    def location(self):
        return self.element.location()

    def rect(self):
        return self.element.rect()

    def screenshot_as_base64(self):
        self.element.screenshot_as_base64()

    def screenshot_as_png(self):
        self.element.screenshot_as_png()

    def screenshot(self, filename):
        self.element.screenshot(filename)

    def parent(self):
        return self.element.parent()

    def id(self):
        return self.element.id()

    def find_element(self, by=By.ID, value=None):
        self.element.find_element(by, value)

    def find_elements(self, by=By.ID, value=None):
        self.element.find_elements(by, value)


class Select:
    def __init__(self, element):
        self.select = Selenium_Select(element)

    def __setitem__(self, key, value):
        self.select[key].select_by_visible_text(value)

    def __getitem__(self, item):
        return self.select[item]

    def set(self, value):
        self.select.select_by_visible_text(value)

    def options(self):
        return self.select.options()

    def all_selected_options(self):
        self.select.all_selected_options()

    def first_selected_option(self):
        self.select.first_selected_option()

    def select_by_value(self, value):
        self.select.select_by_value(value)

    def select_by_index(self, index):
        self.select.select_by_index(index)

    def select_by_visible_text(self, text):
        self.select.select_by_visible_text(text)

    def deselect_all(self):
        self.select.deselect_all()

    def deselect_by_value(self, value):
        self.select.deselect_by_value(value)

    def deselect_by_index(self, index):
        self.select.deselect_by_index(index)

    def deselect_by_visible_text(self, text):
        self.select.deselect_by_visible_text(text)

    def _setSelected(self, option):
        self.select._setSelected(option)

    def _unsetSelected(self, option):
        self.select._unsetSelected(option)
