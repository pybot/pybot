import csv as csv_lib
import os

csv = None


class csvHandle:
    def __init__(self, filePath, fileName, delimiter, headder, encoding):
        self.msgs      = []
        self.filePath  = filePath
        self.fileName  = fileName
        self.path      = filePath + '\\' + fileName
        self.delimiter = delimiter
        self.headder   = headder
        self.encoding  = encoding


def write(data):
    global csv

    with open(csv.path, 'a', newline='', encoding=csv.encoding) as file:
        if csv.headder is not None:
            handler = csv_lib.DictWriter(
                file, delimiter=csv.delimiter, fieldnames=csv.headder)
        else:
            handler = csv_lib.writer(file, delimiter=csv.delimiter)
        csv.msgs.append(data)
        handler.writerow(data)


def write_header():
    global csv

    with open(csv.path, 'a', newline='', encoding=csv.encoding) as file:
        handler = csv_lib.DictWriter(
            file, delimiter=csv.delimiter, fieldnames=csv.headder)
        handler.writeheader()


# def read_file():


def setup(filePath=None, fileName=None, delimiter=';', headder=None, encoding='utf-8'):
    global csv

    if filePath is None:
        filePath = os.getcwd()

    if not os.path.exists(filePath):
        os.makedirs(filePath)

    if fileName is None:
        fileName = 'report.csv'

    csv = csvHandle(filePath, fileName, delimiter, headder, encoding)
