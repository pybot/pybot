import time
import logging
import os
import inspect
from collections import defaultdict

log = None


class Log:
    def __init__(self, filepath=None, level='info'):
        self.msgs = []
        self.filePath = self.setPath(filepath)

        formatter = logging.Formatter('%(message)s')

        # Log de arquivo
        self.logger = logging.getLogger()
        self.logger.setLevel(logLevel(level))
        fileHandler = logging.FileHandler(self.filePath)
        fileHandler.setLevel(logLevel(level))
        fileHandler.setFormatter(formatter)
        self.logger.addHandler(fileHandler)

        # Log de console
        consoleHandler = logging.StreamHandler()
        consoleHandler.setLevel(logLevel(level))
        consoleHandler.setFormatter(formatter)
        self.logger.addHandler(consoleHandler)

    def write(self, msg, level="info"):
        text = ' - '.join(msg)

        if "DEBUG".lower() == level.lower():
            self.logger.debug(text)
        elif "INFO".lower() == level.lower():
            self.logger.info(text)
        elif "WARNING".lower() == level.lower():
            self.logger.warning(text)
        elif "ERROR".lower() == level.lower():
            self.logger.error(text)
        elif "CRITICAL".lower() == level.lower():
            self.logger.critical(text)
        else:
            self.logger.debug(text)

    def setPath(self, pathFile):

        logPath = os.getcwd() + '\\log'

        if pathFile is not None:
            logPath = pathFile

        if not os.path.exists(logPath):
            os.makedirs(logPath)

        return logPath + '\\execution-' + time.strftime("%d-%m-%Y") + '.log'


def logLevel(x):
    return defaultdict(lambda: logging.NOTSET, {
        '': logging.NOTSET,
        'DEBUG': logging.DEBUG,
        'INFO': logging.INFO,
        'WARNING': logging.WARNING,
        'ERROR': logging.ERROR,
        'CRITICAL': logging.CRITICAL
    })[x]


# Metodos usados no import do Log

def write_line(msg, level="info"):

    global log
    if not isinstance(log, Log):
        log = Log()

    date_time = time.strftime("%d/%m/%y - %H:%M:%S")
    caller = inspect.stack()[1][3]

    content = [date_time, caller, level, msg]
    log.msgs.append(content)
    log.write(content, level)


def close():
    pass
    # metodo para finalizar o log
    # if html then cria html
    # if pdf  then cria pdf


def setup(file=None, level='info'):
    global log
    log = Log(filepath=file, level=level)
