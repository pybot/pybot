import sys
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


def send_email(subject="Default_Subject", to=None, body="Default body."):
    try:
        msg = MIMEMultipart('alternative')
        msg["Subject"] = subject
        msg["From"] = ""
        part = MIMEText(body, "html")
        msg.attach(part)
        conn = smtplib.SMTP()
        conn.connect("", port=25)
        conn.set_debuglevel(False)
        conn.ehlo()
        try:
            conn.sendmail("", to, str(msg))
        except Exception as e:
            print(e.message)
        finally:
            conn.close()
    except Exception:
        sys.exit("Failed to send email.")
